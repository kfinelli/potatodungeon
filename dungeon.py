from PIL import Image
import random

class SquareGridMap:
    def __init__(self, size):
        self.size = size
        self.grid = [[None for _ in range(size)] for _ in range(size)]
        self.connections = [[0 for _ in range(size)] for _ in range(size)]

    def dfs(self, x,y):
        if not self.is_valid_position(x,y) or self.is_room_present(x,y):
            return
        self.add_room(x,y)
        for dx, dy in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
            self.dfs(x+dx, y+dy)

    #### add/manipulate rooms
    def init_rooms(self, n_rooms):
        if len(self.empty_cells()) < n_rooms:
            print(f"Not enough empty cells to add {n} points")
            return

        #find a valid starting point
        start_x, start_y = random.choice(self.empty_cells())
        self.add_room(start_x, start_y)
        n_rooms -= 1

        while n_rooms > 0:
            current_x, current_y = random.choice(self.empty_cells())
            neighbors = self.get_neighbors(current_x, current_y)
            if any(self.is_room_present(x,y) for x,y, in neighbors):
                self.add_room(current_x, current_y)
                n_rooms -= 1

    def add_room(self, x, y):
        if self.is_valid_position(x, y):
            self.grid[x][y] = "Room "

    #### get grid data
    def empty_cells(self):
        return [(x, y) for y in range(self.size) for x in range(self.size) if self.is_empty(x,y)]

    def get_neighbors(self, x, y):
        return [(x+dx, y+dy) for dx, dy in [(0,1), (1,0), (0,-1), (-1,0)] if self.is_valid_position(x+dx, y+dy)]

    #### read room/grid info
    def is_room_present(self, x, y):
        return self.grid[x][y] == "Room "

    def is_empty(self, x, y):
        return self.grid[x][y] == None

    def is_valid_position(self, x, y):
        return 0 <= x < self.size and 0 <= y < self.size

    #### utility/other stuff
    def add_connecting_rooms(self, x1, y1, x2, y2):
        if self.is_valid_position(x1, y1) and self.is_valid_position(x2, y2):
            if self.is_room_present(x1,y1) and self.is_room_present(x2,y2):
                return True
        return False

    ### room connections
    def add_connections(self):
        for y in range(self.size):
            for x in range(self.size):
                if self.is_room_present(x,y):
                    possible_neighbors = [(x, y-1), (x, y+1), (x-1, y), (x+1,y)]
                    for i, (nx, ny) in enumerate(possible_neighbors):
                        if self.is_valid_position(nx,ny) and self.is_room_present(nx,ny):
                            self.connections[nx][ny] += 2**i

    #### output/printing
    def print_map(self):
        for row in self.grid:
            print(" ".join([str(cell) if cell else "Empty" for cell in row]))

    def print_connections(self):
        for row in self.connections:
            print(" ".join([str(cell) for cell in row]))

    def create_map_image(self, room_images):
        if len(room_images) != self.size * self.size:
            print("Error: Number of room images doesn't match the grid size.")
            return

        cell_size = max(max(img.size for img in room_images))

        total_width = self.size * cell_size
        total_height = self.size * cell_size

        map_image = Image.new('RGBA', (total_width, total_height))

        for y in range(self.size):
            for x in range(self.size):
                room_image = room_images[y * self.size + x]

                if room_image:
                    map_image.paste(room_image, (x * cell_size, y * cell_size))

        map_image.show()

# Example Usage:
map_size = 9
map_instance = SquareGridMap(map_size)

tile0 = Image.open('tile0.png')
tileA = Image.open('tileA.png')
tileB = Image.open('tileB.png')
tileC = Image.open('tileC.png')
tileD = Image.open('tileD.png')
tileE = Image.open('tileE.png')

#room_images = [tile0, tileD, tileE, tile0, tileC, tileE, tile0, tile0, tileB]
#map_instance.create_map_image(room_images)

map_instance.init_rooms(16)
map_instance.add_connections()
map_instance.print_map()
map_instance.print_connections()
